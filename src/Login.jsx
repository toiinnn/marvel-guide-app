import React, { Component } from 'react';
import {
  Modal,
} from 'semantic-ui-react';
import marvelService from './services/marvel';
import './Login.css'


export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      comicsModal: false,
    };
  }

  loadSectionData = async (sectionName) => {
    try {
      if(sectionName === 'comicsModal') {
        const { data } = await marvelService.getComics();
        const comics = data.data.results;
      }
    } catch (error) {
      console.log(error.message);
    }
  }

  handleOpen = (sectionName) => {
    this.setState({ [sectionName]: true });
    this.loadSectionData(sectionName);
  }

  handleClose = (sectionName) => this.setState({ [sectionName]: false });

  render() {
    return (
      <div className="container">
        <div className="wrapper">
          <div className="section" onClick={() => this.handleOpen('comicsModal')}>
            <p className="section__title">COMICS</p>
          </div>
          <div className="section">
            <p className="section__title section__title--small">CHARACTERS</p>
          </div>
          <div className="section">
            <p className="section__title">STORIES</p>
          </div>
          <div className="section">
            <p className="section__title">SERIES</p>
          </div>
        </div>
        <Modal
          open={this.state.comicsModal}
          onClose={() => this.handleClose('comicsModal')}
          centered={false}
          style={{backgroundColor: 'black'}}
        >
          <Modal.Header>Comics</Modal.Header>
          <Modal.Content>
          </Modal.Content>
        </Modal>
      </div>
    );
  }
}
