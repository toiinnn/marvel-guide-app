import axios, { queryParams } from './index';

const getComics = () => axios.get(`/comics?ts=${queryParams().ts}&apikey=${queryParams().apikey}&hash=${queryParams().hash}`);

export default {
  getComics,
};
