import axios from 'axios';
import md5 from 'md5';

const API_KEY = '6708ea5c7cfa16eccb81b0f749f04426';
const PRIVATE_KEY = '0056fad2fdbac3c5d94ac3cbe19a1ed64bf25615';
const TIMESTAMP = new Date().getTime();

axios.defaults.baseURL = 'https://gateway.marvel.com/v1/public';

export const queryParams = () => ({
  ts: TIMESTAMP,
  apikey: API_KEY,
  hash: md5(TIMESTAMP + PRIVATE_KEY + API_KEY),
});

export default axios;
