<img src="https://www.liberty.edu/champion/wp-content/uploads/2019/04/Marvel_Studios_logo.svg_.png" >

# Marvel guide

Frontend React-based application for Marvel guide project.

## Getting Started

Following this guide you'll be able to execute the application on a local environment and start development right away.

### Prerequisites

To start, you'll need installed on your machine:

- Node v10.15.1 LTS/Dubnium
- NPM v6.5.0

Install dependencies:

```bash
npm install
```

Run local:

```bash
npm start
```

The application will run in development mode. To access, open on your browser:

`localhost:3000`

## Tests

To launch the test suite, run:

`npm test`

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>

See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Built with

- [Node](https://nodejs.org)
- [NPM](https://www.npmjs.com/)
- [React](https://reactjs.org/)